// console.log("Hello World")

// [section] Exponent operator

// before ES6
	const firstNum = 8**2;
	console.log(firstNum);

//ES6
	const secondNum = Math.pow(8,2);
	console.log(secondNum);

//[section] Template Literals
/*
	-allows to write strings without using concatenation operator(+)
	-greatly helps with code readability
*/

	let name = "John";
	// before template literal string
	// use single quote or double quote

	let message = 'Hello ' + name + '! Welcome to programming';

	console.log(message);

	//using template literal
	//uses backticks(``);
message = `Hello 
${name}! 
Welcome 
to 
programming!`
console.log(message);

// template literals allow us to write strings with embedded Javascript

const interestRate = 0.1;
const principal = 1000;
console.log(`The interest on your savings acount is: ${interestRate*principal}.`)

// [section] Array Destructuring
/*
	-allows us to unpack elements in arrays into distict variables
	-allows us to name array elements with variables
	-it will help us with code readability

	Syntax:
	let/const [variableNameA, variableNameB, ...]= arrayName;
*/

const fullName = ['Juan', 'Dela', 'Cruz'];
// Before array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

// Array destructuring

const[firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(fullName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)

//[Section] Object Destructuring
/*
	-allows us to unpack properties of objects into distinct variables

	Syntax:
		-array literal to object literal
		let/const{ propertyNameA, propertyNameB, propertyNameC, ...} = objectName;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
// Before the object destructuring
console.log(person.givenName)
console.log(person["maidenName"]);
console.log(person.familyName);
// Object destructuring
let {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);


function getFullName({givenName, maidenName, family}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}
getFullName(person);

// [section] Arrow Functions
	//Compact alternative syntax to traditional function.

	//useful  for code snippets where creating functions will not be reused in any other portion of code

const hello = () =>{
	console.log("Hello World!");
}
	
// Function Expression
/*const hello = function(){
	console.log("Hello Wolrd!");
}*/
	hello();
// before arrow function and Template literals
	// "Function Declaration"
	function printFullName(firstName, middleInitial, lastName){
		console.log(firstName+ ' '+ middleInitial+ ' '+ lastName);
	}

	printFullName("Chris",'O.', 'Mortel');

	let fName = (firstName, middleInitial, lastName) =>{
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}
	fName("Chris",'O.', 'Mortel');

	//Arrow functions with loops

	const student = ["John", "Jane", "Judy"];
	//before arrow function
		//for every element nung student array, makapagconsole.log "studentName is a student."

	function iterate(student){
		console.log(student + " is a student!")
	}
	student.forEach(iterate);

	// arrow function
	student.forEach((student) =>{
		console.log(`${student} is a student`)
	})


	//[Section] Implicit Return Statement

	/*
		there are instatnces when you can comit return statement this works because even without return statement Javascript implicitly adds it for the result of the function
	*/

	// wala narereturn na value pag may curly braces
	const add = (x,y) => {
		console.log(x+y);
		return x+y;
	}
	let sum = add(23,45);
	console.log("This is the sum contain in sum varaiable: ")
	console.log(sum);

	// one liner - matic may return kahit walang return statements
	const subtract = (x,y) => x-y;
	subtract(10,5);
	let difference = subtract(10, 5);
	console.log(difference);


	//[Section] Default Function Argument value
	// provide a default argument value if none is provided when the function is invoked.

	const greet = (name = "user") =>{
		return `Good morning, ${name}!`;
	}
	console.log(greet());

//[section] Class-based Object two prints
// Allow us to create/instantiation of object using classes blueprints

// Creating class
	//constructor is a special method of a class for creating/initializing an object for that class.
/*
	syntax:
	class className{
	constructor (objectValueA, objectValueB,...){
	this.objectPropertyA = objectValueA;
	this.objectPropertyB = objectValueB;
	}
	}
*/

	class car{
		construtor(bran, name, year){
			this.carBrand = brand;
			this.carName = name;
			this.carYear = year;
		}
	}
	let car = new Car("Toyota", "Hilux-pickup",'2015');
	console.log(car);

	car.carbrand = "Nissan";
	console.log(car);
